/*
A. Get location that we want to see photos of
  1. use Geolocation API to get coordinates (lat and lon) or use a fallback location
    - https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API
    - https://developer.mozilla.org/en-US/docs/Web/API/Geolocation
B. Get photo info from Flickr
  1. use fetch() to send a GET request to flickr.com/services/rest
    - Include the lat and lon
    - Include a search term
  2. Process the promises to get the photo data
    - Convert JSON to a usable object ("rehydrate")
    - Send the photo data to a display function
C. Display photos
  1. Create the image URLs from the photo data (function)
    - https://www.flickr.com/services/api/misc.urls.html
  2. Insert an <img> tag into the page
    - crate an img element
    - set src attribute to image URL
    - append the element to the right place in the document
  3. Display link to the image's Flickr page (function)
    - (Same stuff as the img tag)
D. Provide a way to advance through photos
*/
//Thanks to Amanda Yonce and Howard Post for helping understand the code

let latitude = ""
let longitude = ""
let picNum = 0
let pageNum = 1
let imageUrlArray = []

const findCoords = function() {
    //function to find users location
    function success(position) {
        latitude = position.coords.latitude
        longitude = position.coords.longitude
        console.log(`Latitude: ${latitude} ", Longitude: ${longitude}"`)

    }

    function error() {
        latitude = 52.519121
        longitude = 13.400167
        console.log(`Latitude: ${latitude} ", Longitude: ${longitude}"`)

    }

    if (!navigator.geolocation) {
        latitude = 52.519121
        longitude = 13.400167

    } else {
        navigator.geolocation.getCurrentPosition(success, error)
    }
}

const assembleSearchUrl = function() {
    findCoords()
    return `https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=3b9e025ddb6cee92cd1a61ee220d50cb&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&${latitude}&${longitude}&text=monuments`
}

/*const assemblePhotoPageUrl = function(photoObj) {
    return `https://flickr.com/photos/${photoObj.owner}/${photoObj.id}`
}*/

const fetchPhotos = function() {
    const url = assembleSearchUrl()
    console.log(url)
        //let photoArray = {}
    fetch(url)
        .then(function(response) {
            return response.json()
        })
        .then(function(data) {
            createImageUrl(data)
            return data
        })

    // const photoPageUrl = assemblePhotoPageUrl(photoArray[0])
    //console.log(photoPageUrl)
    //console.log(photoArray)
}

const createImageUrl = function(data) {
    for (let i = 0; i < data.photos.photo.length; i++) {
        imageUrlArray.push("https://farm" + data.photos.photo[i].farm + ".staticflickr.com/" + data.photos.photo[i].server + "/" + data.photos.photo[i].id + "_" + data.photos.photo[i].secret + ".jpg")
    }
    sendImageToHtml(imageUrlArray)
    return imageUrlArray
}
let i = 0
let sendImageToHtml = function(imageUrlArray) {
    if (i < 5) {
        const imageLocation = document.getElementById("photos")
        imageLocation.src = imageUrlArray[i]

        const urlLocation = document.getElementById("photoUrl")
        urlLocation.href = imageUrlArray[i]

        i++
    } else {
        i = 0
        sendImageToHtml(imageUrlArray)
    }
}

fetchPhotos()